package status

const (
	// 保存任务事件
	JOB_EVENT_SAVE = 1
	// 删除任务事件
	JOB_EVENT_DELETE = 2
	// 强杀任务事件
	JOB_EVENT_KILL = 3
)
