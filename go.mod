module wheel

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/go-ini/ini v1.51.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/jinzhu/gorm v1.9.11
	github.com/julienschmidt/httprouter v1.3.0
	github.com/mongodb/mongo-go-driver v1.1.3
	github.com/pkg/errors v0.8.1
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	github.com/tidwall/pretty v1.0.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.1.3 // indirect
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65
	golang.org/x/text v0.3.2
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	google.golang.org/appengine v1.6.5 // indirect
)
